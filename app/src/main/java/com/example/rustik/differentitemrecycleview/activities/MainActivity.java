package com.example.rustik.differentitemrecycleview.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.rustik.differentitemrecycleview.R;
import com.example.rustik.differentitemrecycleview.adapters.RecyclerViewAdapter;
import com.example.rustik.differentitemrecycleview.models.Fio;
import com.example.rustik.differentitemrecycleview.models.Icon;
import com.example.rustik.differentitemrecycleview.models.Info;
import com.example.rustik.differentitemrecycleview.models.Model;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Model> data = new ArrayList<Model>();

        data.add(new Icon(1,"Facebook",R.drawable.fb));
        data.add(new Icon(1,"Facebook2",R.drawable.fb));
        data.add(new Icon(1,"Facebook3",R.drawable.fb));
        data.add(new Fio(3,"Rustam","Akbaraliyev","",""));
        data.add(new Info(2,"Mirjalol","Norqulov"));
        data.add(new Info(2,"Sherzodbek","Muhammadiyev"));
        data.add(new Info(2,"Xurshid","Tursunov"));
        data.add(new Fio(3,"Rustam","Akbaraliyev","",""));
        data.add(new Fio(3,"Rustam","Akbaraliyev","",""));
        data.add(new Icon(1,"Facebook3",R.drawable.fb));
        data.add(new Fio(3,"Rustam","Akbaraliyev","",""));
        data.add(new Fio(3,"Rustam","Akbaraliyev","",""));
        data.add(new Info(2,"Rustam","Akbaraliyev"));
        data.add(new Info(2,"Mirjalol","Norqulov"));
        data.add(new Info(2,"Sherzodbek","Muhammadiyev"));
        data.add(new Info(2,"Xurshid","Tursunov"));

        Collections.shuffle(data);

        RecyclerView recyclerView=(RecyclerView) findViewById(R.id.recycler);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(data);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
    }
}
