package com.example.rustik.differentitemrecycleview.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rustik.differentitemrecycleview.R;
import com.example.rustik.differentitemrecycleview.models.Icon;
import com.example.rustik.differentitemrecycleview.models.Info;
import com.example.rustik.differentitemrecycleview.models.Model;

import java.util.ArrayList;

/**
 * Created by Rustik on 12.10.2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Model> Data;

    public RecyclerViewAdapter(ArrayList<Model> data) {
        Data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Model a = Data.get(viewType);
        if (viewType==2) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fio, parent, false);
            return new ViewHolder1(view);
        }
        if(viewType==1){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_icon, parent, false);
            return new ViewHolder2(view);

        }
        else {
            View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_info,parent,false);
            return  new ViewHolder3(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Model a = Data.get(position);
        Info b = null;

        if  (a.getType()==1) {
            ViewHolder2 viewholder2 = (ViewHolder2) holder;
                  }
        if(a.getType()==2){
            ViewHolder1 viewholder1 = (ViewHolder1) holder;

        }
        if(a.getType()==3) {
            ViewHolder3 v = (ViewHolder3) holder;

        }


    }


   static class ViewHolder1 extends RecyclerView.ViewHolder {

        public ViewHolder1(View itemView) {
            super(itemView);
                }
    }

   static class ViewHolder2 extends RecyclerView.ViewHolder {

        public ViewHolder2(View itemView) {
            super(itemView);
        }
    }

    static class ViewHolder3 extends RecyclerView.ViewHolder {
        public ViewHolder3(View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return Data.get(position).getType();
    }

}
