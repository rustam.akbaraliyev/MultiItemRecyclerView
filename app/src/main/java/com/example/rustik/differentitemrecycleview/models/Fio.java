package com.example.rustik.differentitemrecycleview.models;

/**
 * Created by Rustik on 13.10.2017.
 */

public class Fio extends Model {
    private String title, header, btn_t, bnt_f;

    public Fio(int type, String title, String header, String btn_t, String bnt_f) {
        super(type);
        this.title = title;
        this.header = header;
        this.btn_t = btn_t;
        this.bnt_f = bnt_f;
    }

    public String getTitle() {
        return title;
    }

    public String getHeader() {
        return header;
    }

    public String getBtn_t() {
        return btn_t;
    }

    public String getBnt_f() {
        return bnt_f;
    }
}
