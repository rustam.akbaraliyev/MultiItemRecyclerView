package com.example.rustik.differentitemrecycleview.models;

/**
 * Created by Rustik on 12.10.2017.
 */

public class Icon extends Model {
    private String name;
    private int photo;

    public Icon(int type, String name, int photo) {
        super(type);
        this.name = name;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
