package com.example.rustik.differentitemrecycleview.models;

/**
 * Created by Rustik on 12.10.2017.
 */

public class Info extends Model {
    private String name;
    private String surname;

    public Info(int type, String name, String surname) {
        super(type);
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
