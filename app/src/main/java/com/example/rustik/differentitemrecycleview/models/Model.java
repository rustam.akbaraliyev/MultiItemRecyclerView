package com.example.rustik.differentitemrecycleview.models;

/**
 * Created by Rustik on 12.10.2017.
 */

public class Model {
    private int type;

    public Model(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
